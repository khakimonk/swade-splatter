# SWADE Splatter


## Fork

This is a fork of the excellent Splatter module released by [theripper93](https://github.com/theripper93/Splatter) for the SWADE system

The main difference is in the threshold setting.

The settings can be 0-3, and a splatter will trigger for a token whos wounds go above the threshold. the default is 0.


## Licensing

The included Splatter font was created by [Codin Repsh]( https://www.dafont.com/profile.php?user=362757)

Original concept by [edzillion](https://github.com/edzillion/blood-n-guts)
